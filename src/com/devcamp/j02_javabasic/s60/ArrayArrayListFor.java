package com.devcamp.j02_javabasic.s60;

import java.util.ArrayList;

public class ArrayArrayListFor {
    public static void longArrayList() {
        ArrayList < Integer > numbers = new ArrayList < Integer > ();
        long a[] = new long[100];
        for (int bI = 0; bI < 100; bI++) {
            a[bI] = bI;
            numbers.add(bI);
        }
        System.out.println(numbers);
    }


    public static void doubleArrayList() {
        ArrayList < Integer > numberDouble = new ArrayList < Integer > ();
        double b[] = new double[100];
        for (int i = 0; i < 100; i++) {
            b[i] = i;
            numberDouble.add(i);
        }
        System.out.println(numberDouble);
    }


    public static void main(String[] agrs) {
        ArrayArrayListFor.longArrayList();
        ArrayArrayListFor.doubleArrayList();
    }
}
